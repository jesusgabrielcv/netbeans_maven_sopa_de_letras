/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;






/**
 *
 * @author madarme
 */
public class Matriz_Excel_Ejemplo {
    
    /**
     * @param args the command line arguments
     */
    /*
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        leerExcel();
    }*/
    private String nombreArchivo;
    public Matriz_Excel_Ejemplo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
     public String leerExcel() throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(this.nombreArchivo));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
       // System.out.println("Filas:"+canFilas);
        String palabrasexcel="";
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
            
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
            String valor=filas.getCell(j).getStringCellValue();
            palabrasexcel+=""+valor;
            //System.out.print(valor+"\t");
        }
        if (i<canFilas-1)palabrasexcel+=",";
    // System.out.println();
       }
        //System.out.println(palabrasexcel);
        return palabrasexcel;
    }
    public String getNombreArchivo() {
        return nombreArchivo;
    }
    
    
}
