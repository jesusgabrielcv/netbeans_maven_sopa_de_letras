package Negocio;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras extends DefaultTableCellRenderer
{
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

    
    
    
    
    
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
        //Crear la matriz con las correspondientes filas:
        
     String palabras2[]=palabras.toLowerCase().split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
       
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    public int lenghtsopascolums(){
    return sopas[sopas.length-1].length;
    }
    public int lenghtsopasFilas(){
    return sopas.length;
    }
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    public void imprimirSopa(){
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                System.out.print(sopas[i][j] + "\t");
            }
            System.out.println();
        }
      
    }

    
    public boolean esCuadrada(){
        boolean esCuadrada = true;
        if(this.sopas != null){
            int filas = this.sopas.length;
            int columnas = 0;
            for(int i = 0 ; i<this.sopas.length && esCuadrada;i++){
                columnas = this.sopas[i].length;
                if(filas == columnas){
                    esCuadrada = true;
                }
                else{
                    esCuadrada = false;
                }
            }       
        }
        return esCuadrada;
    }
    
    
     public boolean esDispersa(){
        boolean esDispersa = false;
        if(!esCuadrada() && !esRectangular()) esDispersa = true;
        return esDispersa;
    }

    
    public boolean esRectangular(){
        boolean esRectangular =  true;
        if(this.sopas != null){
            int filas =  this.sopas.length;
            int columnas1 = 0;
            int columnas2 = 0;
            for(int i = 0 ; i<this.sopas.length-1 && esRectangular;i++){
                columnas1= this.sopas[i].length;
                columnas2 = this.sopas[i+1].length;
                if(filas != columnas1 && columnas1 == columnas2){
                    esRectangular =  true;
                }
                else{
                    esRectangular = false;
                }
            }
        }
        return esRectangular;
    }
    
    public char []getDiagonalSuperior() throws Exception
    {
        char diagonal [];
        //si y solo si es cuadrada , si no, lanza excepcion
        if(!esCuadrada()) throw new Exception("La matriz no es cuadrada");
        int tamaño = 0;
        int filas  =  this.sopas.length;
        for(int i=0; i<filas;i++){
            tamaño += (filas-i) - 1;
        }
        diagonal= new char [tamaño];
        int x = 0;
        for(int i=0;i<this.sopas.length;i++){
            for (int j=i;j<this.sopas[i].length-1;j++)
            {
                diagonal[x]= sopas[j+1][i];
                x++;
            }
        }
        return diagonal;
    }
    public char []getDiagonalInferior() throws Exception
    {
        char diagonal [];
        //si y solo si es cuadrada , si no, lanza excepcion
        if(!esCuadrada()) throw new Exception("La matriz no es cuadrada");
        int tamaño = 0;
        int filas  =  this.sopas.length;
        for(int i=0; i<filas;i++){
            tamaño += (filas-i) - 1;
            //System.out.println(tamaño);
        }
        diagonal= new char [tamaño];
        int x = 0;
        for(int i=0;i<this.sopas.length;i++){
            for (int j=i;j<this.sopas[i].length-1;j++)
            {
                diagonal[x]= sopas[i][j+1];
                x++;
            }
        }
        return diagonal;
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int n1, int inicio,int end){
    JLabel cell=(JLabel) super.getTableCellRendererComponent(table,value,isSelected,hasFocus,n1,inicio);
      
    for (int i = inicio; i < sopas[n1].length; i++) {
            
            
        }
    return null;
    }
    public boolean existeIzqDer(String palabra){
    return !posicionesIzqDer(palabra).isEmpty();
    }
    public String doBuscar(String palabra) throws Exception{
        String msg="se encontró en" +" \n" ;
        if(!posicionesIzqDer(palabra).isEmpty())msg += "Izquierda a Derecha Fila, ColumnaInicio, ColumnaFinal" +" \n" + posicionesIzqDer(palabra);
        if(!posicionesDerIzq(palabra).isEmpty())msg+= "Derecha a Izquierda Fila, ColumnaFinal, ColumnaInicio" +" \n" + posicionesDerIzq(palabra);
        if(!posicionesArrAba(palabra).isEmpty())msg+= "Arriba a Abajo Columna, FilaInicio, FilaFinal" +" \n" + posicionesArrAba(palabra);
        if(!posicionesAbaArr(palabra).isEmpty())msg+= "Abajo a Arriba Columna, FilaFinal, FilaInicio" +" \n" + posicionesAbaArr(palabra);
         if(!posicionesDiagonalesPrincipales(palabra).isEmpty()) msg += "Diagonales Principales : X1,Y1,X2,Y2"+" \n" +posicionesDiagonalesPrincipales(palabra);
 if(!posicionesDiagonalesSecundarias(palabra).isEmpty()) msg += "Diagonales Secundarias: X1,Y1,X2,Y2"+" \n" +posicionesDiagonalesSecundarias(palabra);
        return msg;
    }
    public String posicionesIzqDer(String palabra){
        int x = 1;
        int posicionx = 0;
        int posiciony = 0;
        String msg="";
        String imprimir = "";
        for(int i=0;i<this.sopas.length;i++){
            for (int j=0;j<this.sopas[i].length;j++)
            {
             msg+=this.sopas[i][j];
            }
            if(msg.contains(palabra)){
              posicionx = msg.indexOf(palabra)+1;
              posiciony = posicionx + palabra.length()-1;
              imprimir += (i+1)+"," + posicionx +"," +posiciony +" \n";
              x++;
             } 
            msg="";
        }
        return imprimir;
    }
     public String posicionesDerIzq(String palabra)throws Exception {
        if(palabra.length()>this.sopas[0].length)throw new Exception("La palabra es mas larga que las columnas");
        int x = 1;
        int posicionx = 0;
        int posiciony = 0;
        String msg="";
        String imprimir = "";
        for(int i=0;i<this.sopas.length;i++){
            for (int j=this.sopas[i].length-1;j>=0;j--)
            {
             msg+=this.sopas[i][j];
            }
            if(msg.contains(palabra)){
              posicionx = (this.sopas[i].length - 1) - msg.indexOf(palabra);
              posiciony = posicionx - (palabra.length()-1);
              imprimir += (i+1)+"," + (posiciony+1) +"," +(posicionx+1) +" \n";
              x++;
             } 
            msg="";
        }
        return imprimir;
    }    
     
      public String posicionesArrAba(String palabra){
        int x = 1;
        int posicionx = 0;
        int posiciony = 0;
        String msg="";
        String imprimir = "";
        for(int i=0;i<this.sopas.length;i++){
            for (int j=0;j<this.sopas[i].length;j++)
            {
             msg+=this.sopas[j][i];
            }
            if(msg.contains(palabra)){
              posicionx = msg.indexOf(palabra)+1;
              posiciony = posicionx + palabra.length()-1;
              imprimir += (i+1)+"," + posicionx +"," +posiciony +" \n";
              x++;
             } 
            msg="";
        }
        return imprimir;
    }
     public String posicionesAbaArr(String palabra)throws Exception {
        if(palabra.length()>this.sopas[0].length)throw new Exception("La palabra es mas larga que las columnas");
        int x = 1;
        int posicionx = 0;
        int posiciony = 0;
        String msg="";
        String imprimir = "";
        for(int i=0;i<this.sopas.length;i++){
            for (int j=this.sopas[i].length-1;j>=0;j--)
            {
             msg+=this.sopas[j][i];
            }
            if(msg.contains(palabra)){
              posicionx = (this.sopas[i].length - 1) - msg.indexOf(palabra);
              posiciony = posicionx - (palabra.length()-1);
              imprimir += (i+1)+"," + (posiciony+1) +"," +(posicionx+1) +" \n";
              x++;
             } 
            msg="";
        }
        return imprimir;
    }    
    public String posicionesDiagonalesSecundarias(String palabra)throws Exception {
        if(palabra.length()>this.sopas[0].length || palabra.length()>this.sopas.length)throw new Exception("La palabra es mas larga que las columnas y filas de la sopa");
        int posicionx1;
        int posiciony1;
        int posicionx2;
        int posiciony2;
        int columna =(palabra.length()-1);
        String msg="";
        String imprimir = "";
        for(int i=columna;i<this.sopas[0].length;i++){
         int tmpColumna = i;
         int fila = 0;
         for(int j = tmpColumna;j>=0;j--){
          msg += this.sopas[fila][j];
          fila++;
         }
         if(msg.contains(palabra)){
            posicionx1 = msg.indexOf(palabra);
            posiciony1 = i - posicionx1;
            posicionx2 = posicionx1 + (palabra.length() - 1);
            posiciony2  = posiciony1 - (palabra.length() - 1);
            imprimir += (posicionx1+1)+","+(posiciony1+1)+","+(posicionx2+1) +","+(posiciony2+1)+" \n"; 
             } 
          msg="";
        }
        imprimir += posicionesDiagonalesSecundariasFilas(palabra);  
        return imprimir;
        }
    private String posicionesDiagonalesSecundariasFilas(String palabra){
        int posicionx1;
        int posiciony1;
        int posicionx2;
        int posiciony2;
        int fila = (this.sopas.length-1)-(palabra.length()-1);
        int columna = this.sopas[0].length-1;
        String msg = "" ;
        String imprimir = " ";
        for(int i=fila;i>=1;i--){
         int tmpFila = i;
         int tmpColumna = columna;
         for(int j = tmpFila; j<this.sopas.length;j++){
            msg+= this.sopas[j][tmpColumna];
            tmpColumna--;
            }
         if(msg.contains(palabra)){
            posicionx1 = msg.indexOf(palabra) + i;
            posiciony1 = columna - msg.indexOf(palabra);
            posicionx2 = posicionx1 + (palabra.length() - 1);
            posiciony2  = posiciony1 - (palabra.length() - 1);
            imprimir += (posicionx1+1)+","+(posiciony1+1)+","+(posicionx2+1) +","+(posiciony2+1)+" \n"; 
             } 
          msg="";
        }
       return imprimir;
    } 
    public String posicionesDiagonalesPrincipales(String palabra)throws Exception {
        if(palabra.length()>this.sopas[0].length || palabra.length()>this.sopas.length)throw new Exception("La palabra es mas larga que las columnas y filas de la sopa");
        int posicionx1;
        int posiciony1;
        int posicionx2;
        int posiciony2;
        int columna =(this.sopas[0].length-1)-(palabra.length()-1);
        String msg="";
        String imprimir = "";
        for(int i=columna;i>=0;i--){
         int tmpColumna = i;
         int fila = 0;
         for(int j = tmpColumna;j<this.sopas.length;j++){
          msg += this.sopas[fila][j];
          fila++;
         }
         if(msg.contains(palabra)){
            posicionx1 = msg.indexOf(palabra);
            posiciony1 = posicionx1 + i;
            posicionx2 = posicionx1 + (palabra.length() - 1);
            posiciony2  = posiciony1 + (palabra.length() - 1);
            imprimir += (posicionx1+1)+","+(posiciony1+1)+","+(posicionx2+1) +","+(posiciony2+1)+" \n"; 
             } 
          msg="";
        }
        imprimir += posicionesDiagonalesPrincipalesFilas(palabra);
        return imprimir;
        }
     private String posicionesDiagonalesPrincipalesFilas(String palabra){
        int posicionx1;
        int posiciony1;
        int posicionx2;
        int posiciony2;
        int fila = (this.sopas.length-1)-(palabra.length()-1);
        int columna = 0;
        String msg = "" ;
        String imprimir ="";
        for(int i=fila;i>=1;i--){
         int tmpFila = i;
         int tmpColumna = columna;
         for(int j = tmpFila; j<this.sopas.length;j++){
            msg+= this.sopas[j][tmpColumna];
            tmpColumna++;
            }
         if(msg.contains(palabra)){
            posicionx1 = msg.indexOf(palabra) + i;
            posiciony1 = columna + msg.indexOf(palabra);
            posicionx2 = posicionx1 + (palabra.length() - 1);
            posiciony2  = posiciony1 + (palabra.length() - 1);
           imprimir += (posicionx1+1)+","+(posiciony1+1)+","+(posicionx2+1) +","+(posiciony2+1)+" \n"; 
             } 
          msg="";
        }
       return imprimir; 
    }  
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
    public int getContar(String palabra)
    {
        return 0;
    }
    
    
    /*
        debe ser cuadrada sopas
       */
    public char []getDiagonalPrincipal() throws Exception
    {
        if(!esCuadrada()) throw new Exception("La matriz no es cuadrada");
        int i=0;
        char []v=new char[sopas.length];
        
        for(char c[]:sopas){
        v[i]=c[i];
        i++;
        }
    return v;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
