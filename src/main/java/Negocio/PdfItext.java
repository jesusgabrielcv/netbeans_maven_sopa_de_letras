
package Negocio;
import Negocio.Coordenadas;
import Vista.Matriz_Excel_Ejemplo;
import Negocio.SopaDeLetras;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfSpotColor;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.SpotColor;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import java.awt.Color;
import java.io.*; 
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 * Example of using the iText library to work with PDF documents on Java, 
 * lets you create, analyze, modify and maintain documents in this format.
 * Ejemplo de uso de la librería iText para trabajar con documentos PDF en Java, 
 * nos permite crear, analizar, modificar y mantener documentos en este formato.
 *
 * @author xules You can follow me on my website http://www.codigoxules.org/en
 * Puedes seguirme en mi web http://www.codigoxules.org
 */
public class PdfItext {
    // Fonts definitions (Definición de fuentes).
    private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.BOLDITALIC);
    private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
        
    private static final Font categoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static final Font subcategoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static final Font blueFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);    
    private static final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    
    private static final String iTextExampleImage ="";
    /**
     * We create a PDF document with iText using different elements to learn 
     * to use this library.
     * Creamos un documento PDF con iText usando diferentes elementos para aprender 
     * a usar esta librería.
     * @param pdfNewFile  <code>String</code> 
     *      pdf File we are going to write. 
     *      Fichero pdf en el que vamos a escribir. 
     */
    SopaDeLetras s1;
    Matriz_Excel_Ejemplo m1;
    private char sopas[][];
    public void SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
        //Crear la matriz con las correspondientes filas:
        
     String palabras2[]=palabras.toLowerCase().split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
       
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
 
    public void createPDF(File pdfNewFile, Integer rows, Integer columns, String x, String XandY) {
        // We create the document and set the file name.        
        // Creamos el documento e indicamos el nombre del fichero.
        String XandY2[]=XandY.split("\n");
        String XandY3[]=new String[XandY2.length*4];
      
        try {
            Document document = new Document();
            try {

                PdfWriter.getInstance(document, new FileOutputStream(pdfNewFile));

            } catch (FileNotFoundException fileNotFoundException) {
                System.out.println("No such file was found to generate the PDF "
                        + "(No se encontró el fichero para generar el pdf)" + fileNotFoundException);
            }
            document.open();
            // We add metadata to PDF
            // Añadimos los metadatos del PDF
            document.addTitle("Exportamos la tabla a PDF");
            document.addSubject("usando iText");
            document.addKeywords("Java, PDF, iText");
            document.addAuthor("Código gabriel , breiner  ");
            document.addCreator("Código gabriel , breiner");
            
           
            // How to use PdfPTable
            // Utilización de PdfPTable
            
            // We use various elements to add title and subtitle
            // Usamos varios elementos para añadir título y subtítulo
            Anchor anchor = new Anchor("Sopa de letras ", categoryFont);
            anchor.setName("Con busqueda de palabras");            
            Chapter chapTitle = new Chapter(new Paragraph(anchor), 1);
            Paragraph paragraph = new Paragraph("muestra donde se encuentran las palabras", subcategoryFont);
            Section paragraphMore = chapTitle.addSection(paragraph);
            paragraphMore.add(new Paragraph(""));
            paragraphMore.add(new Paragraph(" "));
            Integer numColumns = columns;
            Integer numRows = rows;
            // We create the table (Creamos la tabla).
            PdfPTable table = new PdfPTable(numColumns); 
            for (int row = 0; row < numRows; row++) {
                for (int column = 0; column < numColumns; column++) {
                    Paragraph letra = new Paragraph(String.valueOf(sopas[row][column]));
                    PdfPCell cell = new PdfPCell(letra);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorder(Rectangle.BOTTOM | Rectangle.RIGHT | Rectangle.TOP |Rectangle.LEFT);
                    cell.setBackgroundColor(new BaseColor(Color.yellow));
                    table.addCell(cell);
                    
                }
            }
            
            // We add the table (Añadimos la tabla)
            paragraphMore.add(table);
            paragraphMore.add(new Paragraph(" "));
            paragraphMore.add(new Paragraph(" "));
            paragraphMore.add(new Paragraph(x));
            // We add the paragraph with the table (Añadimos el elemento con la tabla).
            document.add(chapTitle);
            document.close();
            JOptionPane.showMessageDialog(null,"Se ha generado tu hoja PDF!");
        } catch (DocumentException documentException) {
            JOptionPane.showMessageDialog(null,"Se ha producido un error al generar un documento)" + documentException);
        }
    }
    /**
     * @param args the command line arguments
     */
    /*public static void main(String args[]) {
        GeneratePDFFileIText generatePDFFileIText = new GeneratePDFFileIText();
        generatePDFFileIText.createPDF(new File("1.pdf"));
    }*/
}
